
# 3dsMax 动画重定向 v0.451 | 2021.12.22


*	更新替换蒙皮骨骼权重功能

---

## 视频教程

*   [0.45更新](https://www.bilibili.com/video/BV1Eq4y1m7L6)

*   [替换蒙皮骨骼权重](https://www.bilibili.com/video/BV1R3411r7Vh/)

此教程是用拾取好之后的角色模板来快速替换蒙皮骨骼，因为涉及到添加蒙皮骨骼操作，所以要确保模型是蒙皮姿势


*   [恐龙FBX动画转bip](https://www.bilibili.com/video/BV1Mr4y117gw/)

该视频是将一个无模型的恐龙FBX动画资源转成biped动画，涉及到分批次重定向动画，身体和尾巴分两次处理。因为没有模型，无法恢复到蒙皮姿势，所以拾取模板之后自动创建biped骨架不太准确，需要手动调整biped体型，创建biped尾骨骼，保存匹配pose保存角色模板，与之FBX骨骼姿势匹配。随后是重定向动画给biped，完成FBX动画转bip。

*   [mixamo模型套bip动画_P1](https://www.bilibili.com/video/BV14V411Y7J6/)

从mixamo下载一个带骨骼蒙皮的模型，拾取模板之后自动创建出biped骨骼，保存匹配pose保存角色模板，选择biped骨骼加载一个跳舞动画bip,最后将biped动画烘焙给mixamo下载的模型骨骼上。

*   [mixamo动画转bip_p2](https://www.bilibili.com/video/BV14V411Y7J6?p=2)


从mixamo下载一个带模型的跳舞动画FBX，拾取模板之后自动创建出biped，保存匹配pose保存角色模板，然后将FBX动画烘焙给biped骨骼，完成FBX 动画转bip动画。

*   [Rokoko动捕库_p3](https://www.bilibili.com/video/BV14V411Y7J6?p=3)

Rokoko动捕库中的免费FBX动画资源(第一帧Tpose,无模型)，拾取模板之后自动创建出biped，保存匹配pose保存角色模板，然后将FBX动画烘焙给biped骨骼，完成FBX 动画转bip动画。

*   [Rokoko批量处理_p4](https://www.bilibili.com/video/BV14V411Y7J6?p=4)

批量重定向多个Rokoko动捕库中的免费FBX动画资源(第一帧Tpose,无模型)，继续使用P3视频中创建biped和角色模板，给biped骨骼添加一个原点根骨骼root,然后选择biped骨骼导出FBX，此biped_FBX 为目标骨架。然后选择角色模板和目标骨架，添加FBX动画资源，执行Rokoko免费FBX动画批量转为 biped_FBX动画资源。

*   [Rokoko批量处理_复制动画类型_p5](https://www.bilibili.com/video/BV14V411Y7J6?p=5)

该视频演示了不同的动画复制类型在动画重定向之后的动画效果不一同之处。


*   [FBX动画复制](https://www.bilibili.com/video/BV1r541157hs?p=1)

该视频演示 两个FBX模型之间在保存匹配pose保存角色模板之后互相复制动画。将UE4的小白人动画重向给自己项目的FBX角色。

*   [biped_FBX骨骼重建biped](https://www.bilibili.com/video/BV1r541157hs?p=2)

该视频演示，从一个原本是biped骨骼的FBX模型上拾取模板后自动创出原体型的biped骨骼。

*   [其他FBX骨骼重建biped](https://www.bilibili.com/video/BV1r541157hs?p=3)

该视频演示，从一个其他命名标准和轴向骨骼FBX模型上拾取模板后自动创出原体型的biped骨骼。

上面这些视频都是大同小异，无论什么骨骼，流程都是一样的，更多是需要了解模板的作用。(mixamo的资源都有两个模板，注意是模板上的骨骼名称是不是和你下载的资源一样)


善熊熊这个B站账号因无法实名制所以弃用，转为使用 4698to 这个喵头像账号更新视频，请关注新账号及时了解新情况。


---

![](https://gitee.com/to4698/anim_edit/raw/master/000456.png)


## 2021.6.28 本工具为免费工具

另除了CGjoy外，我没在其他地方有卖插件服务，大家有发现可疑的卖家可以自行举报或是联系我。

下图中的淘宝店 - 好视界:some1222  与本人无关，是店家无礼的倒卖行为(好气 为什么有人愿意为这工具白送钱给店家,却没人给我捐赠呢)

![](https://gitee.com/to4698/anim_edit/raw/master/IMG/110300.png)


---

帮助文档请看项目 wiki (因为一些奇怪的原因无法直接给连接)

![](https://gitee.com/to4698/anim_edit/raw/master/F4C0F1F18A80.png)


---

## 使用


1.   拾取蒙皮骨骼或者绑定控制器 到自定义面板 中
2.   拾取另外一角色的 蒙皮骨骼 或者 绑定控制器 到控制面板 中
3.   保存匹配Pose
4.   保存自定义模板(可选步骤)
5.   保存控制模板(可选步骤)
6.   设置复制动画类型(默认除质心外全为复制旋转)
7.   保存角色模板

完成以上 5 步，即可在自定义角色和控制角色之间互相 复制动画

---

## 关于动画重定向的时间顺序

1.	质心
2.	pelvis 盆骨
3.	lleg 左腿
4.	rleg 右腿
5.	spine 脊椎
6.	neck 脖子
7.	larm 左手
8.	rarm 右手
9.	lfingers1 左边手指
10.	lfingers2
11.	lfingers3
12.	lfingers4
13.	lfingers5
14.	rfingers1 右手指
15.	rfingers2
16.	rfingers3
17.	rfingers4
18.	rfingers5
19.	ltoes1 左脚趾
20.	ltoes2
21.	ltoes3
22.	ltoes4
23.	ltoes5
24.	rtoes1 右脚趾
25.	rtoes2
26.	rtoes3
27.	rtoes4
28.	rtoes5

当用巨多骨骼动画重定向而图形按钮不够用时，按可此部位顺序来拾取骨骼而不是完全按人体来拾取。

----
## 详细文档

0.42版本新加了 FBXSDK ,安装相对多了一个步骤,请仔细看安装说明。


---

## 百度云下载


*   3dsMax 2015-2016
*   3dsMax 2017
*   3dsMax 2018-2019
*   3dsMax 2020


链接：https://pan.baidu.com/s/1oakAx_enc_C957trwY-BOQ
提取码：0b0w




自行选择对应版本下载,(创建Biped功能只能在英文版本MAX中使用)

-----

群号：109185941

点击链接加入群聊【天晴工具组】：https://jq.qq.com/?_wv=1027&k=YJSgv3fe

E-Mail: 738746223@qq.com

99U : 199505